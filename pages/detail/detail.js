// pages/detail/detail.js
var WxParse = require('../../wxParse/wxParse.js')
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    var id = options.id;
    console.log('id:'+id)
    var baseUrl = app.globalData.baseUrl;
    wx.request({
      url: baseUrl+'/article/getOne/'+id,
      success:function(repson){
        var data = repson.data.data;
        var title = data.title;
        if(title.length>15){
          title = title.substring(0,15) +"...";
        }
        wx.setNavigationBarTitle({
          title: title
        })
        var wxml = WxParse.wxParse('content', 'html', data.contentHtml, that, 0);
        // console.log(wxml);
        that.setData({
          article : data
        })

      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})