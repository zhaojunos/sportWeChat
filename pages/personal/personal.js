// pages/personal/personal.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    disabled: 'disabled',
    disabled1: true,
    disabled2: true,
    disabled3: true,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var baseUrl = app.globalData.baseUrl;
    var sessionkey = wx.getStorageSync('sessionkey');
    var that = this;
    this.getMyUserInfo(this, baseUrl, sessionkey);
  },
  getMyUserInfo: function (that, baseUrl, sessionkey){
    wx.request({
      url: baseUrl + '/user/wx/getUserInfo?sessionKey=' + sessionkey,
      success: function (resp) {
        var user = resp.data.data;
        that.setData({
          id: user.id,
          realName: user.realName,
          phone: user.phone,
          address: user.address,
          openid: user.openid
        })
      }
    })
  },
  usernameInput:function(e){
    var realName = this.data.realName;
    if (!realName){
      realName = "";
    } 
    if (e.detail.value =="") {
      this.setData({
        disabled: 'disabled'
      });
      return;
    }
    if (e.detail.value != realName){
      this.setData({
        disabled1:false,
        newRealName: e.detail.value
      })
    }else{
      this.setData({
        disabled1: true,
        newRealName: realName
      })
    }
    this.isDisabled();
  },
  phoneInput:function(e){
    var phone = this.data.phone;
    if (!phone) {
      phone = "";
    } 
    if (e.detail.value == "") {
      this.setData({
        disabled: 'disabled'
      });
      return;
    }
    if (e.detail.value != phone) {
      this.setData({
        disabled2: false,
        newPhone: e.detail.value
      })
    } else {
      this.setData({
        disabled2: true,
        newPhone: phone
      })
    }
    this.isDisabled();
  },
  addsInput:function(e){
    var address = this.data.address;
    if (!address) {
      address = "";
    } 
    if (e.detail.value == "") {
      this.setData({
        disabled: 'disabled'
      });
      return;
    }
    if (e.detail.value != address) {
      this.setData({
        disabled3: false,
        newAddress: e.detail.value
      })
    } else {
      this.setData({
        disabled3: true,
        newAddress: address
      })
    }
    this.isDisabled();
  },
  isDisabled:function(){
    var disabled1 = this.data.disabled1;
    var disabled2 = this.data.disabled2;
    var disabled3 = this.data.disabled3;
    if (!disabled1 || !disabled2 || !disabled3 ){
      this.setData({
        disabled:''
      })
    }else{
      this.setData({
        disabled: 'disabled'
      })
    }
    if (disabled1){
      this.setData({
        newRealName: this.data.realName
      })
    }
    if (disabled2) {
      this.setData({
        newPhone: this.data.phone
      })
    }
    if (disabled3) {
      this.setData({
        newAddress: this.data.address
      })
    }
  },
  doEdit:function(){
    var baseUrl = app.globalData.baseUrl;
    var sessionkey = wx.getStorageSync('sessionkey');
    var that = this;
    var openid = this.data.openid;
    var reqData = {
      id: that.data.id,
      realName: that.data.newRealName,
      phone: that.data.newPhone,
      adds: that.data.newAddress,
      openid: openid
    }
    wx.request({
      url: baseUrl + '/user/wx/edit/user',
      data: reqData,
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded'
      },
      success: function (e) {
        console.log(e.data);
        if (e.data.statusCode == 200) {
          wx.showModal({
            title: '',
            content: '修改成功',
            showCancel: false
          })
          that.getMyUserInfo(that, baseUrl, sessionkey);
          that.setData({
            disabled: 'disabled'
          })
        }
      }
    })
  }
})