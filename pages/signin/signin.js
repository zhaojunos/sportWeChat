// pages/signin/signin.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var baseUrl = app.globalData.baseUrl;
    var sessionkey = wx.getStorageSync('sessionkey');
    var that = this;
    wx.request({
      url: baseUrl + "/punch/wx/punRanking?sessionKey=" + sessionkey,
      success:function(res){
        that.setData({
          punCounts:res.data.data
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  warnFunction:function(){
    var baseUrl = app.globalData.baseUrl;
    wx.request({
      url: baseUrl+'/warn/wechat/first',
      success:function(data){
        var content = data.data.data.warnString;
        wx.showModal({
          title: '温馨提示',
          content: content,
          showCancel: false
        })
      }
    })
    
  }
})