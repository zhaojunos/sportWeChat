//index.js
//获取应用实例
const app = getApp()

Page({
  data: {

  },
  count: 0,
  onLoad: function () {
    console.log('onload')
    var that = this;
    app.globalData.page = that;
  },

  click: function (event){
  var postId = event.currentTarget.dataset.postId;
  wx.navigateTo({
    url: '/pages/detail/detail?id=' + postId,
   })
  },

  onShow:function(){
    this.myRequest();
  },
  myRequest:function(){
    var that = this;
    var baseUrl = app.globalData.baseUrl
    wx.request({
      url: baseUrl +'/article/carousels',
      success:function(e){
        var data = e.data;
        if (data.statusCode == 200 && data.data){
          that.setData({
            carousels: data.data
          });
        }
      }
    })

    wx.request({
      url: baseUrl + '/article/wechat/articles?page=0',
      success: function (e) {
        if (e.data.statusCode == 200) {
          that.setData({
            articles: e.data.data
          });
        }
      }
    });
  },
  page: 0,
  onLower:function(){
    var that = this;
    this.page = this.page + 1;
    var baseUrl = app.globalData.baseUrl
    var temp = [];
    var articles = that.data.articles;
    wx.request({
      url: baseUrl + '/article/wechat/articles?page='+that.page,
      success: function (e) {
        if (e.data.statusCode == 200) {
          temp = e.data.data
          for (var index in temp){
            var subject = temp[index];
            var title = subject.title;
            if(title.length >= 26){
              title = title.substring(0,26)+"...";
            }
            var t = {
              title:title,
              id:subject.id,
              author: subject.author,
              describe: subject.describe,
              overhead: subject.overhead,
              deleted: subject.deleted,
              contentHtml: subject.contentHtml,
              coverImageUrl: subject.coverImageUrl,
              contentWxml: subject.contentWxml,
              carousel: subject.carousel,
              createTime: subject.createTime,
              updateTime: subject.updateTime,
              deleteTime: subject.deleteTime
            }
            articles.push(t);
          }
          that.setData({
            articles: articles
          })
        }
      }
    });
  }
})
