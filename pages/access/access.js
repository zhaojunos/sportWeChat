// pages/access/access.js
const app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    getUserInfoFail: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    wx.getUserInfo({
      success: function (res) {
        var userInfo = res.userInfo;
        console.log(userInfo);
        app.globalData.userInfo = userInfo;
        app.globalData.getUserInfoFail = false;
        wx.switchTab({
          url: '../index/index',
        })
      }
    });
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },
  authTap: function () {
    this.count++;
    var that = this;
    if (wx.openSetting) {
      wx.openSetting({
        success: function (res) {
          wx.getUserInfo({
            success: function (res) {
              var userInfo = res.userInfo;
              console.log(userInfo);
              app.globalData.userInfo = userInfo;
              wx.switchTab({
                url: '../index/index',
              })
            }
          })
        },
        fail: function () {
          console.log('授权失败');
        }
      })
    } else {
      wx.showModal({
        title: '授权提示',
        content: '小程序需要您的微信授权才能使用哦~ 错过授权页面的处理方法：删除小程序->重新搜索进入->点击授权按钮'
      })
    }
  },
})