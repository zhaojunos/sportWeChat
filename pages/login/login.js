// pages/login/login.js
var app = getApp();

Page({

  /**
   * 页面的初始数据
   */
  data: {
    username: '',
    phone: '',
    adds: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    app.globalData.page = that;
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  },
  usernameInput: function (e) {
    this.setData({
      username: e.detail.value
    })
  },
  phoneInput: function (e) {
    this.setData({
      phone: e.detail.value
    })
  },
  addsInput: function (e) {
    this.setData({
      adds: e.detail.value
    })
  },
  doRequest: function () {
    var that = this;
    var userInfo = app.globalData.userInfo;
    var baseUrl = app.globalData.baseUrl;
    var sessionkey = wx.getStorageSync('sessionkey');
    wx.request({
      url: baseUrl + '/user/openid?sessionKey=' + sessionkey,
      success: function (res) {
        var data = res.data;
        var openid = data.data;
        var respData = {
          realName: that.data.username,
          phone: that.data.phone,
          adds: that.data.adds,
          nickName: userInfo.nickName,
          gender: userInfo.gender,
          province: userInfo.province,
          avatarUrl: userInfo.avatarUrl,
          openid: openid
        }
        var baseUrl = app.globalData.baseUrl;
        wx.request({
          url: baseUrl + '/user/wx/save/user',
          data: respData,
          method: 'POST',
          header: {
            'Content-Type': 'application/x-www-form-urlencoded'
          },
          success: function (e) {
            console.log(e.data);
            if (e.data.statusCode == 200){
              console.log(e.data.statusCode)
              wx.switchTab({
                url: '../index/index',
              })
            }
          }
        })
      }
    })
  }
})