// pages/signinshare/signinshare.js

var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isPun: false,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this;
    app.globalData.page = that;
    var baseUrl = app.globalData.baseUrl;
    var sessionKey = wx.getStorageSync('sessionkey');
    wx.request({
      url: baseUrl + '/punch/punchIsToDay?sessionKey=' + sessionKey,
      success:function(res){
        var data = res.data.data;
        if (res.data.data == 1){
          that.setData({
            isPun : true
          })
        }
      }
    })
    wx.request({
      url: baseUrl +'/punch/history?sessionKey='+sessionKey,
      success:function(res){
        var punchData = res.data.data;
        console.log(punchData)
        that.setData({
          punchData: punchData
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },
  //打卡点击事件
  singTap: function () {
    var that = this;
    //未打卡时激活
    var isPun = this.data.isPun;
    if (!isPun) {
      var baseUrl = app.globalData.baseUrl;
      var sessionkey = wx.getStorageSync('sessionkey');
      wx.request({
        url: baseUrl + '/user/openid?sessionKey=' + sessionkey,
        success: function (res) {
          var data = res.data;
          var openid = data.data;
          wx.request({
            url: baseUrl + "/punch/punch?openid=" + openid,
            success: function (res) {
              if (res.data.statusCode == 200) {
                that.setData({
                  isPun: true
                });
                wx.request({
                  url: baseUrl + '/punch/history?sessionKey=' + sessionkey,
                  success: function (res) {
                    var punchData = res.data.data;
                    console.log(punchData)
                    that.setData({
                      punchData: punchData
                    });
                    //打卡结束后等待3秒  进入打排名页
                    setTimeout(function(){
                      wx.switchTab({
                        url: '../signin/signin',
                      })
                    },3000);
                  }
                });
              }
            }
          })
        }
      })
    }
  }
})