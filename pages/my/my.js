// pages/my/my.js
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
  
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var userInfo = app.globalData.userInfo;
    console.log("userinfo:"+userInfo)
    this.setData({
      userInfo: userInfo
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },
  navigateToSignin:function(){
    wx.switchTab({
      url: '../signin/signin',
    })
  },
  navigateToPersonal:function(){
    wx.navigateTo({
      url:'../personal/personal'
    })
  }
})